
import ipaddress
from Validator.ipexception import IpException


class Ip(object):

    def __init__(self, ip):
        self.ip = self.nomalize(ip)
        try:
            ipaddress.ip_address(self.ip)
        except:
            raise IpException(message='ip format not valid', errors='IpException')

    def nomalize(self, ip):
        ip_nomalize = str(ip)
        ip_nomalize = ip_nomalize.lstrip()
        ip_nomalize = ip_nomalize.rstrip()
        return ip_nomalize

    def __str__(self):
        return str(self.ip)
