import ipaddress
import re
from Validator.ipexception import IpException
from Validator.macexception import MacException


class Mac(object):

    def __init__(self, mac):
        self.mac = self.nomalize(mac)
        self.mac = self.mac.replace(':', '', -1)
        self.mac = self.mac.replace('-', '', -1)
        self.mac = self.mac.upper()
        self.mac = ':'.join([self.mac[i: i + 2]
                             for i in range(0, len(self.mac), 2)])
        if not re.match("[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", self.mac.lower()):
            raise MacException(message='mac format not valid',
                               errors='MacException')

    def nomalize(self, mac):
        self.mac = str(mac)
        self.mac = self.mac.lstrip()
        self.mac = self.mac.rstrip()
        return self.mac

    def __str__(self):
        return self.mac
