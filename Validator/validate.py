from getpass import getpass
from Logger.loger import Loger
import ipaddress
import re, sys
from Validator.ipexception import IpException

LOG = Loger('logs/logsys.log', 'speedtest')



class Validate(object):

    def valid_ip(self, address):
        try: 
            print("ip ", ipaddress.ip_address(address))
            return True
        except:
            raise IpException('ip format not valid','')
    def valid_ip_gui(self, address):
           try: 
               print("ip ", ipaddress.ip_address(address))
               return True
           except:
               return False

    def yes_or_no(self, question):
        while True:    
            reply = str(input(question+' (y/n): ')).lower().strip()
            if len(reply) > 0:
                if reply[0] == 'y':
                    return True
                if reply[0] == 'n':
                    return False
            return False