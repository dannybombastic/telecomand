#!venv/bin/python3  # -*- coding: utf-8 -*-

from __init__ import *
from Menu.menu import Menu
choice_menu = Menu()

def add_arguments():

    parser = argparse.ArgumentParser(description='Process some data')
    parser.add_argument('--mac',
                        dest='mac', help='Get vendor from MAC')
    parser.add_argument('--rem',
                        dest='rem', help='Remove user by id')
    parser.add_argument('--c', action='store_true',
                        dest='create', help='Create user with encrypted pass and secret key')
    parser.add_argument('--s', action='store_true',
                        dest='menu', help='Start selection menu')
    parser.add_argument('--u', action='store_true',
                        dest='users', help='List all user info')                        
    
 
    return parser.parse_args()


def main():
    args = add_arguments()
  
    choice_menu.create_menu(args)


@atexit.register
def goodbye():
  
    print(f'{WARNING} \nYou are now leaving the Python sector.{ENDOC}')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        choice_menu.delete_driver()
        pass
