
# main script
from __init__ import *

menu = f"""
    0. {BLUE}Retype Ip and Mac ?{ENDOC}
    1. {BLUE}Ssh speedtest ?{ENDOC}
    2. {BLUE}Open Ubiquiti WEBUI ?{ENDOC} 
    3. {BLUE}Open Albentia WEBUI ?{ENDOC}
    4. {BLUE}Open with winbox ?{ENDOC}
    5. {RED}Exit/Quit or press enter <¬ ?{ENDOC}
    """


header = f'''

       {UNDERLINE}Welcome to:{ENDOC} \U0001F30D  
       ______                          __   
      / ____/___  ____  ________  ____/ /   
     / /   /    \/ __ \/ ___/ _ \/ __  /    
    / /___/ \U0001F4E1  / / / / /  /  __/ /_/ /     
    \____/\____/_/ /_/_/   \___/\__,_/ \U0001F5A5      
    
 '''