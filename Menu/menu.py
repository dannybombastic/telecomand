
from Menu.__init__ import *


class Menu(object):

    def show_menu(self):
        print(menu)

    def show_header(self):
        print(header)

    def mac_captcha(self):
        validate = Validate()
        mac = validate.yes_or_no(
            'Do you need MAC Addres selector if not press enter <¬ to continue (Y/N)\n\n --> ')

        while mac:
            try:
                mac = input(f'\n Mac ? \n -->  ')
                mac = Mac(mac)
                vendor, mac = api.get_vendor(mac)
                mac = False
                return vendor
            except MacException as e:
                print(f'{WARNING}   wrong Mac Format {mac} {ENDOC}')
        return 'mac not set'

    def ip_captcha(self):
        ip_flag = True
        while ip_flag:
            try:
                ip = input(f'\n Ip ?  \n --> ')
                ip = Ip(ip)
                ip_flag = False
                return ip
            except IpException as e:
                print(f'{WARNING}   wrong Ip Format {ip} {ENDOC}')

    def create_menu(self, args):
        ip = None
        self.webmanager = WebManager()
        if args.create:
            self.show_header()
            user = security.aks_for_user_create()
            return
        if args.users:
            self.show_header()
            security.get_all_user()
            return
        if args.rem:
            self.show_header()
            security.delete_user(args)
            security.get_all_user()
            return
        if args.menu:
            ans = True
            self.show_header()
            self.show_menu()
            vendor = self.mac_captcha()
            ip = self.ip_captcha()
            self.show_menu()
            while ans:
                ans = input(
                    f'\n\n{GREEN}What would you like to do ? ip : {str(ip)} mac :  {vendor} {ENDOC}\n\n -->  ')

                if ans == '0':
                    self.show_menu()
                    vendor = self.mac_captcha()
                    ip = self.ip_captcha()
                if ans == '1':
                    name, key, port, tech = security.read_user()
                    if vendor:
                        ssh = ssh_client.antena_selector(
                            str(ip), port, name, key, vendor)
                        if ssh:
                            ssh_client.exec_command('ls -l /')
                            ssh_client.exec_command('pwd')
                            ssh_client.close()
                            self.show_menu()
                            continue

                        continue
                    ssh_client.antena_selector(domain_ip=str(
                        ip), port=port, username=name, password=key)
                    ssh_client.exec_command('ls -l /')
                    ssh_client.exec_command('pwd')
                    ssh_client.close()
                    self.show_menu()
                elif ans == '2':
                    self.webmanager.open_ubnt(f'https://{str(ip)}:22443')
                    print('\n Ubiquiti opened webui')
                    self.show_menu()
                elif ans == '3':
                    self.webmanager.open_cambium(f'https://{str(ip)}:22443')
                    print(
                        '\n Cambium opened webui \n Press log-out button to avoid cache system')
                    self.show_menu()
                elif ans == '4':
                    name, key = security.read_user()
                    subprocess.run(
                        ['./winbox/winbox.exe', {ip}, {name}, {key}])
                    print('\n winbox opened')
                elif ans == '5':
                    print('\n Goodbye')
                    ans = False
                elif ans != '':
                    print('\n Not Valid Choice Try again')
                    self.show_menu()
        if args.mac:

            device, mac_vendor = api.get_vendor(get_mac_address(ip=args.mac))
            device = device.split(' ', -1)[0]
            print(device.upper(), mac_vendor)

    def delete_driver(self):
        if self.webmanager:
            del(self.webmanager)
