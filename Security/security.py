

from Security.__init__ import *

db = SqliteDatabase('.sqlite.db')


class BaseModel(Model):
    ''' Base model '''
    class Meta:
        database = db


class User(BaseModel):
    ''' User model '''

    username = CharField(unique=False)
    password = CharField(unique=False)
    port = CharField(unique=False)
    technology = CharField(unique=False)
    key = CharField(unique=False)


class Security:

    def __init__(self, *args):
        ''' secret key provide by user'''

        self.key = None
        db.connect()  # Instaciate database1
        db.create_tables([User])  # run migrations with the model
        self.validate = Validate()  # instance to validate ip, macs...

    def encrypt_message(self, message):
        '''' encrypt message '''

        self.message = message.encode()  # traget message
        enc = Fernet(self.key)  # with the key we can encrypt the message
        return enc(self.message)

    def decrypting_message(self, enc_message):
        '''' decrypt message '''

        user_key = User.get(User.id == 1).key
        enc = Fernet(user_key.encode())
        return enc.decrypt(enc_message.encode())

    def generate_key(self, password):
        '''' generate encrypted pass  '''
        salt = os.urandom(16)
        self.kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        self.key = base64.urlsafe_b64encode(self.kdf.derive(password.encode()))
        return self.key

    def generate_user(self, password, user_name, secret, port, tech):
        ''' save user information'''
        key = self.generate_key(secret)

        f = Fernet(key)
        user = User.create(username=user_name, password=f.encrypt(password.encode()),
                           key=key,
                           port=port,
                           technology=tech
                           )
        user.save()
        return user

    def read_user(self, id=1):
        try:
            user = User.get(User.id == id)
            if user:
                password = user.password
                # print("user_password", self.decrypting_message(
                #     password).decode('utf8'))
                return user.username, self.decrypting_message(password).decode('utf8'), user.port, user.technology
        except DoesNotExist:
            pass
        return False

    def create_user(self):
        LOG.console_info(GREEN_URDERLINE+'Welcome to '+BLUE_UNDERLINE+'CONRED'+ENDOC +
                         LOG.bcolors.OKGREEN+LOG.bcolors.UNDERLINE+' speedtest create a superUser'+ENDOC)
        user_flag = True
        while user_flag:
            username = input(BLUE_UNDERLINE+'Type an username:  '+ENDOC)
            if self.validate.yes_or_no(f'Are the data correct ? username {username} '):
                user_flag = False

        port_flag = True
        while port_flag:
            port = input(BLUE_UNDERLINE+'Type an PORT:  '+ENDOC)
            if self.validate.yes_or_no(f'Are the data correct ? PORT {port} '):
                port_flag = False

        tech_flag = True
        while tech_flag:
            tech = input(BLUE_UNDERLINE+'Type an Technology:  '+ENDOC)
            if self.validate.yes_or_no(f'Are the data correct ? TECH {tech.upper()} '):
                tech_flag = False

        pass_flag = True
        while pass_flag:
            password = getpass(BLUE_UNDERLINE+'Type a password:  '+ENDOC)
            password_clone = getpass(
                BLUE_UNDERLINE+'Retype the password: '+ENDOC)
            if len(password) == 0:
                print(LOG.bcolors.FAIL+'     should have at least one character')
                continue
            if password != password_clone:
                print(LOG.bcolors.FAIL+'    password fail'+ENDOC)
                continue
            print(GREEN+'    password success'+ENDOC)
            pass_flag = False

        secret = input(BLUE_UNDERLINE+'Type an secret: '+ENDOC)
        user = self.generate_user(
            password, username, secret, port, tech.upper())

        if user:
            print(f'{GREEN}  {user.username}  user created success{ENDOC}')

        again_flag = True

    def aks_for_user_create(self):
        while self.validate.yes_or_no(f'Do you want create a new user ? '):
            self.create_user()
            again_flag = False

    def get_all_user(self):
        print(f'''
         Role amdin User list
         ''')
        users = User.select()

        for user in users:
            print(f'''
            id : {user.id}
            username :  {user.username}
            port :  {user.port}
            tech :  {user.technology}
            ''')
        return User.select()

    def delete_user(self, args):
        user = User.get(User.id == int(args.rem))
        name = user.username
        user.delete_instance()
        print(f'User {name} deleted')
