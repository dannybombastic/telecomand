
from Security.__init__ import *


class SshManager(object):

    def antena_selector(self, domain_ip, port, username, password, mac_vendor=''):

        print('mac_adress :', mac_vendor)
        return self.ssh_conecction(domain_ip, port, username, password)

    def ssh_conecction(self, domain_ip, port, username, password):

        validate = Validate()
        ip_flag = validate.valid_ip(domain_ip)
   

        if ip_flag:
            self.client = paramiko.SSHClient()
            self.client.load_system_host_keys()
            self.client.set_missing_host_key_policy(paramiko.WarningPolicy())
            try:
                self.client.connect(hostname=domain_ip, port=port,
                                    username=username, password=password, timeout=30)
                return True                                    
            except paramiko.ssh_exception.AuthenticationException:
                sys.stdout.write('Autentification error')
                return False  
            except paramiko.ssh_exception.NoValidConnectionsError:
                sys.stdout.write('error port number')
                return False  
            except (paramiko.ssh_exception.NoValidConnectionsError, socket.error ) as se:
                sys.stdout.write('host unrechable')
                return False 
            except paramiko.ssh_exception.SSHException:
                 sys.stdout.write('host unrechable wrong ip')
                 return False 
    def exec_command(self, command):
        try:
            stdin, stdout, stderr = self.client.exec_command(command)
            outlines = stdout.readlines()
            resp = ''.join(outlines)
            print(resp)
            return (str(resp))
        except paramiko.ssh_exception.SSHException:
            
            return False , ('Ssh exception error')
            

    def close(self):
        self.client.close()
 
        
