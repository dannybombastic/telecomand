

# Securiti.py
import base64
import os, re, base64, paramiko, datetime, socket, sys
 
from Logger.loger import Loger
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet
from peewee import *
from peewee import IntegrityError
from getpass import getpass
from Validator.validate import Validate
from Validator.validate import Validate
from Macapi.apilookup import ApiLookUp
from Validator.ip import Ip
# setup logging
paramiko.util.log_to_file("logs/ssh.log")

LOG = Loger('logs/logsys.log', 'speedtest')
GREEN_URDERLINE = LOG.bcolors.OKGREEN+LOG.bcolors.UNDERLINE
BLUE_UNDERLINE = LOG.bcolors.OKBLUE+LOG.bcolors.UNDERLINE
ENDOC = LOG.bcolors.OKGREEN+LOG.bcolors.ENDC
BLUE = LOG.bcolors.OKBLUE
GREEN = LOG.bcolors.OKGREEN
