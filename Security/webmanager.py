
import platform

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
UBNT_USERNAME = '//*[@id="loginform-username"]'
UBNT_KEY = '//*[@id="loginform-password"]'
UBNT_LOGOUT = '/html/body/div[1]/div[2]/form/div/button'
CAMBIUM_USERNAME = '//*[@id="login1"]/input[1]'
CAMBIUM_KEY = '//*[@id="login1"]/input[2]'
CAMBIUM_LOGOUT = '//*[@id="main_region"]/div/div[1]/div[2]/div[2]'

ubnt = {
    "USERNAME": '//*[@id="loginform-username"]',
    "KEY":  '//*[@id="loginform-password"]',
    "LOG_OUT": '/html/body/div[1]/div[2]/form/div/button',
    "PASS": 'Con1sis!',
    "USER": 'ubnt',
    "LOGBUTTOM": 'login'
}

cambiuim = {
    "USERNAME": '//*[@id="login1"]/input[1]',
    "CAMBIUM_KEY":  '//*[@id="login1"]/input[2]',
    "LOG_OUT": '//*[@id="main_region"]/div/div[1]/div[2]/div[2]',
    "PASS": 'Con1sis!',
    "USER": 'admin',
    "LOGBUTTOM":  '//*[@id="loginBtn"]'
}

antenas = [ubnt, cambiuim]


class WebManager(object):
    def get_os(self):
        print(platform.system())
        return platform.system()

    def __init__(self):

        self.chrome_options = Options()
        self.chrome_options.add_experimental_option("detach", True)
        self.chrome_options.add_argument('--incognito')
        self.chrome_options.add_argument("--allow-running-insecure-content")
        self.chrome_options.add_argument("--disable-popup-blocking")
        self.os_system = self.get_os()
        self.caps = webdriver.DesiredCapabilities.CHROME.copy()
        self.caps['acceptInsecureCerts'] = True

    def system_selector(self):
       
        if self.os_system == 'Windows':
            self.driver = webdriver.Chrome(desired_capabilities=self.caps, chrome_options=self.chrome_options,
                                           executable_path="./Security/chromedriver.exe")

        if self.os_system == 'Linux':
            try:
                self.driver = webdriver.Chrome(desired_capabilities=self.caps, chrome_options=self.chrome_options,
                                            executable_path="./Security/chromedriver")
            except:
                self.driver = webdriver.Chrome(desired_capabilities=self.caps, chrome_options=self.chrome_options,
                                           executable_path="./Security/chromedriver.exe")
        self.driver.implicitly_wait(5)

    def open_cambium(self, url):

        LOG_OUT = '//*[@id="main_region"]/div/div[1]/div[2]/div[2]'
        try:

            try:

                self.system_selector()

                self.driver.get(url)
                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(
                        (By.XPATH, CAMBIUM_USERNAME))
                )
            except NoSuchElementException:
                print('no element in ubnt')

                return

            self.driver.delete_all_cookies()
            try:

                self.driver.find_element_by_id(
                    'details-button').click()
                self.driver.find_element_by_id('proceed-link').click()
                self.driver.implicitly_wait(3)
            except NoSuchElementException:
                print('accept Insecure Certs from CAMBIUM')

            self.driver.find_element_by_xpath(
                CAMBIUM_USERNAME).send_keys('admin')
            self.driver.find_element_by_xpath(
                CAMBIUM_KEY).send_keys('Con1sis!')
            self.driver.find_element_by_xpath(
                '//*[@id="loginBtn"]').click()

            try:
                element = self.driver.find_element_by_partial_link_text("Home")
            except NoSuchElementException:
                print("incorrect password cambium")

            l_out = EC.visibility_of_element_located((By.XPATH, LOG_OUT))
            if l_out:
                return True
            else:
                return False
        except TimeoutException as e:
            print('timeout cam')
            self.just_type_ubnt_login()
            return False

    def open_ubnt(self, url):

        LOG_OUT = '/html/body/div[1]/div[2]/form/div/button'
        try:

            try:

                self.system_selector()
                self.driver.get(url)

                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, UBNT_USERNAME))
                )
            except NoSuchElementException:
                print('no element in ubnt')

                return

            self.driver.delete_all_cookies()
            try:
                self.driver.find_element_by_id('details-button').click()
                self.driver.find_element_by_id('proceed-link').click()
                self.driver.implicitly_wait(3)
            except NoSuchElementException:
                print('accept Insecure Certs from UBNT')

            self.driver.find_element_by_xpath(
                UBNT_USERNAME).send_keys('ubnt')
            self.driver.find_element_by_xpath(
                UBNT_KEY).send_keys('Con1sis!g')
            self.driver.find_element_by_name(
                'login').click()

            try:
                element = self.driver.find_element_by_partial_link_text(
                    "LOCAL DEVICE")
            except NoSuchElementException:
                print("incorrect password")

            l_out = EC.visibility_of_element_located((By.XPATH, LOG_OUT))
            if l_out:
                return True
            else:
                return False
        except TimeoutException as e:
            print('timeout ubnt')
            self.just_type_cambium_login()
            return False

    def just_type_cambium_login(self):
        self.driver.find_element_by_xpath(
            CAMBIUM_USERNAME).send_keys('admin')
        self.driver.find_element_by_xpath(
            CAMBIUM_KEY).send_keys('Con1sis!')
        self.driver.find_element_by_xpath(
            '//*[@id="loginBtn"]').click()

    def just_type_ubnt_login(self):
        self.driver.find_element_by_xpath(
            UBNT_USERNAME).send_keys('ubnt')
        self.driver.find_element_by_xpath(
            UBNT_KEY).send_keys('Con1sis!')
        self.driver.find_element_by_name(
            'login').click()

    def open_explorer(self, url, next=False):
        usbn = 'ubnt'
        pwbn = 'Con1sis!'
        position = 0
        if next:
            position = 1

        LOG_OUT = antenas[position]['LOG_OUT']
        try:

            try:

                self.system_selector()

                self.driver.get(url)
                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(
                        (By.XPATH, antenas[position]['USERNAME']))
                )
            except NoSuchElementException:
                print('no element in ubnt')

                return

            self.driver.delete_all_cookies()
            try:

                self.driver.find_element_by_id(
                    'details-button').click()
                self.driver.find_element_by_id('proceed-link').click()
                self.driver.implicitly_wait(3)
            except NoSuchElementException:
                print('accept Insecure Certs from CAMBIUM')

            self.driver.find_element_by_xpath(
                antenas[position]['USERNAME']).send_keys( antenas[position]['USER'])
            self.driver.find_element_by_xpath(
                antenas[position]['KEY']).send_keys( antenas[position]['PASS'])
            self.driver.find_element_by_xpath(
                antenas[position]['LOGBUTTOM']).click()

            try:
                element = self.driver.find_element_by_partial_link_text("Home")
            except NoSuchElementException:
                print("incorrect password cambium")

            l_out = EC.visibility_of_element_located((By.XPATH, LOG_OUT))
            if l_out:
                return True
            else:
                return False
        except TimeoutException as e:
            print('timeout cam')
            self.open_explorer(url, True)
            return False

    def __del__(self):
        try:
            if self.driver:
                self.driver.quit()
        except:
            pass
