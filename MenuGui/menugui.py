
from MenuGui.__init__ import *
from PIL import ImageTk, Image


class GUImenu(object):

    def __init__(self, *args, **kwargs):
        self.root = Tk()

        self.mac = ''
        self.root.title('Conred communications')
        # self.callback = self.root.register(self.validate_mac)
        self.input_frame = self.create_frame()

        img = Image.open('MenuGui/logo_conred.png')
        img = img.resize((75, 50), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        label = Label(self.input_frame, image=img)
        label.grid(row=0, column=2, columnspan=4, rowspan=2,
                   sticky=W, padx=5, pady=5)
        label.grid_columnconfigure(0, weight=1)
        self.info_text = StringVar()
        self.ip_text = StringVar()
        self.mac_text = StringVar()
        self.warning_text = StringVar()
        self.warning_text.set('')
        self.ssh_manager = SshManager()

        self.info_text_area = ''
        self.scan_ip = Button(
            self.input_frame, text='Scan IP',  bg='green', fg='white', command=self.scan_ssh)

        # self.mac = Entry(self.input_frame,
        #                  justify=RIGHT, textvariable=self.mac_text, validate='focusout', validatecommand=self.callback)

        self.mac = Entry(self.input_frame,
                         justify=RIGHT, textvariable=self.mac_text)
        self.mac.grid(row=2, column=0, pady=(0, 15))

        self.ip_addres = Entry(self.input_frame,
                               justify=RIGHT, textvariable=self.ip_text)
        self.ip_addres.grid(row=5, column=0)

        self.warning = Label(self.input_frame, textvariable=self.warning_text)

        self.text = StringVar()
        self.mac_input_builder()
        self.ip_input_builder()
        self.info_section()
        self.cambium_input_builder()
        self.ubnt_input_builder()

        self.root.mainloop()

    def validate_ip(self):
        text = self.ip_text.get()
        validate = Validate()
        ip_flags = validate.valid_ip_gui(text)
        print(ip_flags)
        if ip_flags:
            self.scan_ip_button()
            self.show_warning('', show=False)
        else:
            self.show_warning('ip fromat not valid')
            self.scan_ip_button(show=FALSE)

    def validate_mac(self):
        text = self.mac_text.get()
        try:
            mac = str(Mac(text))
            self.scan_mac()
            self.show_warning('', show=False)
        except MacException as e:
            self.show_warning('Mac format not suported', rw=2)

    def show_warning(self, wd, rw=5, show=True):
        if show:
            self.warning_text.set(wd)
            self.warning.grid(row=rw, column=2)
            self.warning.grid_columnconfigure(2, minsize=100)
        else:
            self.warning.grid_forget()

    def create_frame(self):
        input_frame = Frame(self.root, width=640, height=480)
        input_frame.pack(fill='both', expand=1)
        args = {'cursor': 'arrow', 'padx': 20, 'pady': 20}
        input_frame.config(**args)
        return input_frame

    def mac_input_builder(self):
        mac_label = Label(self.input_frame, text='MAC address')
        mac_label.grid(row=1, column=0, sticky='w')

        mac_button = Button(
            self.input_frame, text="click here", command=self.validate_mac)
        mac_button.grid(row=2, column=1, sticky='w', pady=(0, 15))

    def ip_input_builder(self):
        ip = Label(self.input_frame, text='IP address')
        ip.grid(row=4, column=0, sticky='w')

        ip_button = Button(self.input_frame, text='VALIDATE',
                           command=self.validate_ip,   bg='blue', fg='white',)
        ip_button.grid(row=5, column=1)

    def cambium_input_builder(self):

        img = Image.open('MenuGui/ubnt_logo.png')
        img = img.resize((75, 50), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        cambium_button = Button(self.input_frame, text="Cambium WEUI",
                                command=self.validate_ip)
        cambium_button.grid(row=7, column=0)
        cambium_button.config(pady=15)

    def ubnt_input_builder(self):
        ubnt_button = Button(self.input_frame, text='Ubiquiti WEUI',
                             command=self.validate_ip)
        ubnt_button.grid(row=7, column=5)
        ubnt_button.config(pady=15)

    def scan_ip_button(self, show=TRUE):

        if show:
            self.scan_ip.grid(row=5, column=2, sticky='w', padx=5)
        else:
            print("button off")
            try:
                self.scan_ip.grid_forget()
            except TypeError:
                pass

    def scan_ssh(self):
        ''' scan a valip ip '''
        try:
            self.ssh_manager.ssh_conecction(
                self.ip_text.get(), 22, 'rocket', '659011563')
            output = self.ssh_manager.exec_command('ls -l')
            self.info_text_area.insert(INSERT, '\n-- ssh output: \n\n'+output)
        except:
            self.info_text_area.insert(
                INSERT, '\n-- ssh output: \n\n'+'Host not reacheable')

    def scan_mac(self):
        ''' get vendor name '''
        verndor_manager = ApiLookUp()
        vendor, mac = verndor_manager.get_vendor(self.mac_text.get())
        self.info_text_area.insert(INSERT, '\n-- vendor: '+vendor)

    def info_section(self):
        info_frame = Frame(self.root, width=640, height=420)
        info_frame.pack(fill='both', expand=1)
        info_label = Label(info_frame, text='Speedtest Info')
        info_label.grid(row=1, column=0, sticky='s')
        self.info_text_area = Text(info_frame)
        self.info_text_area.grid(row=2, column=0)
        self.info_text_area.config(bg='white')
